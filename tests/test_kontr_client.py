import pytest

from kontr_api import KontrClient


@pytest.fixture()
def instance():
    return KontrClient('localhost')


def test_kontr_client_urls_are_correct(instance):
    assert instance.portal_url == 'localhost'
    assert instance.url == 'localhost/api/v1.0'


def test_kontr_client_auth_is_correct(instance):
    assert instance.auth.url == instance.url

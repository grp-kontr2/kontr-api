import mock
import pytest

from kontr_api import KontrClient


def mocked_requests_request(*args, **kwargs):
    class _Stub(object):
        def json(self):
            return {
                'refresh_token': 'foo',
                'access_token': 'bar'
                }

    return _Stub()


@pytest.fixture()
def instance_username():
    return KontrClient('localhost', username='foo', password='bar')


def test_auth_base_is_correctly_initialized(instance_username):
    auth = instance_username.auth
    assert auth.url == instance_username.url
    assert auth.login_url == auth.url + '/auth/login'
    assert auth.refresh_url == auth.url + '/auth/refresh'


@mock.patch('requests.post', mocked_requests_request)
def test_auth_login_mock(instance_username):
    login = instance_username.auth.login()
    assert login['access_token'] == 'bar'
    assert login['refresh_token'] == 'foo'
